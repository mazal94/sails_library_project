/**
 * BookController
 *
 * @description :: Server-side logic for managing Books
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var runId = 1000;

module.exports = {

    createBook: function(req, res) {

        if (_.isUndefined(req.param('name'))) {
            return res.badRequest('An name is required!');
        }

        if (_.isUndefined(req.param('author'))) {
            return res.badRequest('A author is required!');
        }

        if (_.isUndefined(req.param('edition'))) {
            return res.badRequest('A edition is required!');
        }

        if (_.isUndefined(req.param('copies'))) {
            return res.badRequest('A copies is required!');
        }

        var options = {};


        //options.id = ++runId;
        options.name = req.param('name');
        options.author = req.param('author');
        options.edition = req.param('edition');
        options.copies = req.param('copies');
        options.availableCopies = req.param('copies');

        options.owner = req.session.userId;

        Book.create(options).exec(function(err, createdBook) {
            if (err) {
                console.log('the error is: ', err);

                return res.negotiate(err);
            }

            return res.ok({
                book: createdBook
            });
        });



    },

    removeBook: function(req, res) {

        Book.destroy({
                id: req.param('id')
            })
            .exec(function(err) {

                if (err) {
                    return res.notFound();
                }

                res.ok();
            });
    },

    updateBook: function(req, res) {

        Book.update({
            id: req.param('id')
        }, {
            edition: req.param('edition')
        }, function(err, updatedBook) {

            if (err) return res.negotiate(err);

            return res.json({
                name: updatedBook[0].name
            });

        });
    },

    lendBook: function(req, res) {

        if (_.isUndefined(req.param('book_id'))) {
            return res.badRequest('An book id is required!');
        }

        User.findOne({
            id: req.session.userId
        }, function foundUser(err, user) {
            if (err) return res.negotiate(err);
            if (!user) {
                sails.log.verbose('Session refers to a user who no longer exists.');
                return res.redirect('/');
            }

            Book.findOne({
                    id: req.param('book_id')
                })
                .populate('users')
                .exec(function(err, book) {
                    if (err) return res.negotiate(err);
                    if (!book) {
                        sails.log.verbose('Book no longer exists.');
                        return res.redirect('/');
                    }
                    if (book.availableCopies <= 0) {
                        return res.badRequest('All copies are lend');
                    }

                    //sails.log('book ' + book.users.length);
                    for (var i = 0; i < book.users.length; i++) {

                        //sails.log('book.users[i].id ' + book.users[i].id);
                        //sails.log('user.id ' + user.id);
                        if (book.users[i].id == user.id) {
                            return res.badRequest('You alredy lend the book');
                        }
                    }

                    book.users.add(user.id);
                    book.availableCopies--;
                    book.save(function(err) {
                        if (err) return res.negotiate(err);

                    });

                    return res.ok();
                });
        });
    },

    returnBook: function(req, res) {
        if (_.isUndefined(req.param('book_id'))) {
            return res.badRequest('An book id is required!');
        }

        User.findOne({
            id: req.session.userId
        }, function foundUser(err, user) {
            if (err) return res.negotiate(err);

            if (!user) {
                sails.log.verbose('Session refers to a user who no longer exists.');
                return res.redirect('/');
            }

            Book.findOne({
                    id: req.param('book_id')
                })
                .populate('users')
                .exec(function(err, book) {
                    if (err) return res.negotiate(err);
                    if (!book) {
                        sails.log.verbose('Book no longer exists.');
                        return res.redirect('/');
                    }

                    //sails.log('book ' + book.users.length);
                    var flag = false;
                    for (var i = 0; i < book.users.length; i++) {

                        if (book.users[i].id == user.id) {
                            flag = true;
                        }
                    }

                    if (flag) {
                        //sails.log.info(book.users);
                        book.users.splice(user, 1);
                        //sails.log('books after remove ' + book.users.length);
                        book.availableCopies++;

                        Book.update({
                                id: req.param('book_id')
                            }, {
                                users: book.users
                            })
                            .exec(function(err) {
                                //sails.log("here");
                                //sails.log.info(book.users);
                                if (err) return res.negotiate(err);
                                return res.ok();
                            });
                    }
                    else {
                        return res.badRequest('You didn\'t lend the book');
                    }
                });
        });
    },

    showUsers: function(req, res) {
        Book.findOne({
                id: req.param('book_id')
            })
            .populate('users')
            .exec(function(err, book) {

                if (err) return res.negotiate(err);
                //sails.log('book is '+book.name);
                if (!book) {
                    sails.log.verbose('Book no longer exists.');
                    return res.redirect('/');
                }
                return res.ok({
                    book: book
                });
            });
    },

    getBookList: function(req, res) {
        Book.find(function(err, books) {
            if (err) {
                return res.negotiate(err);
            }

            if (!books) {
                sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');

            }

            return res.ok({
                books: books
            });

        });
    },

    getBooksByOwner: function(req, res) {
        Book.find({
            owner: req.session.userId
        }, function(err, books) {

            if (err) return res.negotiate(err);
            //sails.log('book is '+book.name);
            if (!books) {
                sails.log('Books no exists.');
                return res.ok({
                    book: null
                });
            }

            return res.ok({
                books: books
            });
        });
    },

};
