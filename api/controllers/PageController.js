/**
 * PageController
 *
 * @description :: Server-side logic for managing pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  home: function(req, res) {
    if (!req.session.userId) {
      return res.view('signin', {
        me: null
      });
    }


    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('signin', {
          me: null
        });
      }

      Book.find(function(err, books) {
        if (err) {
          return res.negotiate(err);
        }

        if (!books) {
          sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');

          User.find(function(err, users) {
            if (err) {
              return res.negotiate(err);
            }


            //console.info(books);
            return res.view('homepage', {
              me: {
                username: user.username,
                gravatarURL: user.gravatarURL,
                admin: user.admin
              },
              books: null,
              users: users
            });
          });
        }

        //console.info(books);
        User.find(function(err, users) {
          if (err) {
            return res.negotiate(err);
          }

          //console.info(books);
          return res.view('homepage', {
            me: {
              username: user.username,
              gravatarURL: user.gravatarURL,
              admin: user.admin
            },
            books: books,
            users: users
          });
        });

      });


    });
  },

  signup: function(req, res) {
    return res.view('signup', {
      me: null,
      books: null,
      users: null
    });
  },

  signin: function(req, res) {

    return res.view('signin', {
      me: null
    });
  },

  profile: function(req, res) {

    User.findOne({
        username: req.param('username')
      })
      .exec(function(err, foundUser) {
        if (err) return res.negotiate(err);
        if (!foundUser) return res.notFound();

        User.findOne({
            id: req.session.userId
          })
          .exec(function(err, loggedInUser) {
            if (err) {
              return res.negotiate(err);
            }

            if (!loggedInUser) {
              return res.serverError('User record from logged in user is missing?');
            }

            // We'll provide `me` as a local to the profile page view.
            // (this is so we can render the logged-in navbar state, etc.)
            var me = {
              username: loggedInUser.username,
              email: loggedInUser.email,
              gravatarURL: loggedInUser.gravatarURL,
              admin: loggedInUser.admin
            };

            // We'll provide the `isMe` flag to the profile page view
            // if the logged-in user is the same as the user whose profile we looked up earlier.
            if (req.session.userId === foundUser.id) {
              me.isMe = true;
            }
            else {
              me.isMe = false;
            }

            // Return me property for the nav and the remaining properties for the profile page.
            return res.view('profile', {
              me: me,
              username: foundUser.username,
              gravatarURL: foundUser.gravatarURL
            });
          }); //</ User.findOne({id: req.session.userId})
      });
  },

  editProfile: function(req, res) {

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        console.log('error: ', err);
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('homepage');
      }

      return res.view('edit-profile', {
        me: {
          email: user.email,
          username: user.username,
          gravatarURL: user.gravatarURL,
          admin: user.admin
        }
      });
    });
  },

  logout: function(req, res) {

    if (!req.session.userId) {
      return res.redirect('/');
    }

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        console.log('error: ', err);
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('/');
      }

      return res.view('signout', {
        me: null
      });
    });
  },

  removeProfile: function(req, res) {

    if (!req.session.userId) {
      return res.redirect('/');
    }

    User.destroy({
        id: req.session.userId
      })
      .exec(function(err) {
        if (err) return res.negotiate(err);
        
        req.session.userId = null;
        
        return res.ok();
      });
  },
};
