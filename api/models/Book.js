/**
 * Book.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  
  //connection: 'myMysqlServer',
  
  //migrate: 'alter',

  attributes: {
    name: {
      type: 'string'
    },
    
    author: {
      type: 'string'
    },
    
    edition: {
      type: 'string'
    },
    
    copies: {
      type: 'int'
    },
    
    availableCopies: {
      type: 'int'
    },
    
    owner: {
      model: 'User'
    },
    
    users: {
      collection: 'User',
      via: 'id'
    }

  }
};

