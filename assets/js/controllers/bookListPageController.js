var app = angular.module('library');

app.controller('bookListPageController', function($scope, $http, toastr) {
    $scope.books = window.SAILS_LOCALS.books;

    

    io.socket.on('book', function(e) {
        console.log('new book added!', e);

        // Append the chat we just received    
        $scope.books.push({
            id: e.id,
            name: e.name,
            author: e.author,
            edition: e.edition,
            owner: e.owner
        });

        // Because io.socket.on() is not an angular thing, we have to call $scope.$apply() in
        // this event handler in order for our changes to the scope to actually take effect.
        $scope.$apply();
    });

});
