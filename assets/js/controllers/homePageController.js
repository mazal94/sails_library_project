var app = angular.module('library');

app.controller('homePageController', function($scope, $http, $filter, toastr) {
    $scope.books = window.SAILS_LOCALS.books;
    $scope.lendings = window.SAILS_LOCALS.lendings;
    $scope.users = [];
    $scope.allUsers = window.SAILS_LOCALS.users;

    
    $scope.showUsers = function(book_id) {
        $http.post('/book/showUsers', {
                book_id: book_id
            })
            .then(function onSuccess(sailsResponse) {
                $scope.users = sailsResponse.data.book.users;
                //console.log("here "+ sailsResponse.data.book.users);
                document.getElementById('usersTable').style.display = "block";
            })
            .catch(function onError(sailsResponse) {
                // Otherwise, display generic error if the error is unrecognized.
                toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));

            });
    }

    $scope.hideUsers = function(book_id) {
        $scope.users = [];
        //console.log("here "+ sailsResponse.data.book.users);
        document.getElementById('usersTable').style.display = "none";

    }


    $scope.openTable = function(tableName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        if (tableName=='MainTable')
        {
          document.getElementById('usersTable').style.display = "none";
        }

        if (tableName == 'BooksTable') {
            $http.post('/book/getBookList')
                .then(function onSuccess(sailsResponse) {
                    $scope.books = sailsResponse.data.books;
                })
                .catch(function onError(sailsResponse) {
                    // Otherwise, display generic error if the error is unrecognized.
                    toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));

                });
        }
        if (tableName == 'UsersTable') {
            $http.post('/user/getUserList')
                .then(function onSuccess(sailsResponse) {
                    $scope.allUsers = sailsResponse.data.users;
                })
                .catch(function onError(sailsResponse) {
                    // Otherwise, display generic error if the error is unrecognized.
                    toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));

                });
        }

        // Show the current tab, and add an "active" class to the link that opened the tab
        document.getElementById(tableName).style.display = "block";
    }


    $scope.lendBook = function(id) {
        $http.put('/book/lendBook', {
                book_id: id
            })
            .then(function onSuccess(sailsResponse) {
                toastr.success("Book lended!");
            })
            .catch(function onError(sailsResponse) {
                // Otherwise, display generic error if the error is unrecognized.
                toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));
            });
    }
    
    $scope.returnBook = function(id) {
        $http.put('/book/returnBook', {
                book_id: id
                
            })
            .then(function onSuccess(sailsResponse) {
                toastr.success("Book returned!");
            })
            .catch(function onError(sailsResponse) {
                // Otherwise, display generic error if the error is unrecognized.
                toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));
            });
    }


    $scope.openDialog_book = function() {
        document.getElementById('myPopupDialog_book').style.display = "block";
    }

    $scope.closeDialog_book = function() {
        document.getElementById('myPopupDialog_book').style.display = "none";
    }

    $scope.saveBook = function() {
        $http.post('/book/addBook', {
                name: $scope.g_name_book,
                author: $scope.g_author_book,
                edition: $scope.g_edition_book,
                copies: $scope.g_copies_book
            })
            .then(function onSuccess(sailsResponse) {

                document.getElementById('myPopupDialog_book').style.display = "none";
                toastr.success("Book added!");
                $scope.books.push(sailsResponse.data.book);
                
                $scope.g_name_book = "";
                $scope.g_author_book = "";
                $scope.g_edition_book = "";
                $scope.g_copies_book = "";
            })
            .catch(function onError(sailsResponse) {

                // Handle known error type(s).
                if (sailsResponse.status == 409) {
                    toastr.error(sailsResponse.data);
                    return;
                }
            });
    }


    $scope.openDialog_removeBook = function() {
        document.getElementById('myPopupDialog_removeBook').style.display = "block";

        $http.post('/book/getBooksByOwner')
            .then(function onSuccess(sailsResponse) {
                $scope.booksByUser = sailsResponse.data.books;
            })
            .catch(function onError(sailsResponse) {
                // Otherwise, display generic error if the error is unrecognized.
                toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));

            });
    }

    $scope.closeDialog_removeBook = function() {
        document.getElementById('myPopupDialog_removeBook').style.display = "none";
    }

    $scope.removeBook = function() {
        var val = $(".booksByUser").find("input:radio:checked").val();
        $http.put('/book/removeBook', {
                id: val
            })
            .then(function onSuccess(sailsResponse) {
                toastr.success("Book Deleted!");
                
                for (var i = 0; i < $scope.books.length; i++) {
                    if ($scope.books[i].id == val) {
                        console.log("val " + val);
                        $scope.books.splice(i, 1);
                    }
                }
                document.getElementById('myPopupDialog_removeBook').style.display = "none";
            })
            .catch(function onError(sailsResponse) {
                // Otherwise, display generic error if the error is unrecognized.
                toastr.error('An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status));

            });
    }
    
});
