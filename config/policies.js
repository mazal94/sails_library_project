/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions (`true` allows public     *
  * access)                                                                  *
  *                                                                          *
  ***************************************************************************/

  //'*': true,

	
	PageController: {
		home: [],
		signup: [],
		signin: [],
		profile: ['isLoggedIn'],
		editProfile: ['isLoggedIn'],
		logout: ['isLoggedIn'],
		removeProfile: ['isLoggedIn']
	},
	
	BookController: {
		createBook: ['isLoggedIn'],
		removeBook: ['isLoggedIn'],
		updateBook: ['isLoggedIn'],
		lendBook: ['isLoggedIn'],
		returnBook: ['isLoggedIn'],
		showUsers: ['isLoggedIn'],
		getBookList: ['isLoggedIn'],
		getBooksByOwner: ['isLoggedIn']
	},
	
	UserController: {
		login: [],
		logout: ['isLoggedIn'],
		signup: [],
		generateRecoveryEmail: ['isLoggedIn'],
		resetPassword: ['isLoggedIn'],
		updateProfile: ['isLoggedIn'],
		changePassword: ['isLoggedIn'],
		updateDeleted: ['isLoggedIn'],
		getUserList: ['isLoggedIn']
	}
};
