/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /*************************************************************
  * Server Rendered HTML Page Endpoints                        *
  *************************************************************/
  
  'GET /': 'PageController.home',
  'GET /signup': 'PageController.signup',
  'GET /signin': 'PageController.signin',
  'GET /logout': 'PageController.logout',
  
  'GET /:username': {
    controller: 'PageController',
    action: 'profile',
    skipAssets: true
  },
  'GET /profile/edit': 'PageController.editProfile',


  /*************************************************************
  * JSON API ENDPOINTS                                         *
  *************************************************************/

  'PUT /login': 'UserController.login',
  
  'PUT /user/update-profile': 'UserController.updateProfile',
  'PUT /user/remove-profile': 'PageController.removeProfile',
  'PUT /user/change-password': 'UserController.changePassword',
  'PUT /user/signup': 'UserController.signup',
  'POST /user/getUserList': 'UserController.getUserList',
  
  'POST /logout': 'UserController.logout',
  
  'PUT /book/lendBook': 'BookController.lendBook',
  'PUT /book/returnBook': 'BookController.returnBook',
  'POST /book/showUsers': 'BookController.showUsers',
  'POST /book/addBook': 'BookController.createBook',
  'POST /book/getBookList': 'BookController.getBookList',
  'POST /book/getBooksByOwner': 'BookController.getBooksByOwner',
  'PUT /book/removeBook': 'BookController.removeBook',
  
};
